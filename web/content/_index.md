---
title: Your Good Governance initiative
date: [GGI_CURRENT_DATE]
layout: default
---

# Welcome

{{% content "includes/initialisation.inc" %}}

This is the website of your own good governance Initiative.

There are currently:

{{% content "includes/activities_stats_home.inc" %}}


## Current activities <a href='current_activities' class='w3-text-grey' style="float:right">[ details ]</a> 

Current activities are defined as having the label <span class="w3-tag w3-light-grey">in_progress</span>. <br />
These are your current activities:

{{% content "includes/current_activities.inc" %}}

See the detailed list of [current activities](current_activities).


## Completed activities <a href='past_activities' class='w3-text-grey' style="float:right">[ details ]</a>

Completed activities are defined as having the label <span class="w3-tag w3-light-grey">done</span>. <br />
These are your completed activities:

{{% content "includes/past_activities.inc" %}}

See the detailed list of [completed activities](past_activities).

## About

The [Good Governance Initiative](https://ospo.zone/ggi) is developed by the OSPO Alliance.

Check [our website](https://ospo.zone) and [join the discussion](https://accounts.eclipse.org/mailing-list/ospo.zone)!
